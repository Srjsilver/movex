package no.solve.movex.service;

import no.solve.movex.config.UnitTest;
import no.solve.movex.repository.OrderRepository;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class OrderServiceTest extends UnitTest {

    @InjectMocks
    OrderService orderService;
    @Mock
    OrderRepository orderRepository;

    @Test
    public void testPlaceOrder() {
        orderService.placeOrder(orderAllProducts);
    }
    @Test
    public void testDeleteOrder(){
        orderService.deleteOrder(orderAllProducts.getOrderNumber());
    }
    @Test
    public void testFindOrderByNameSearch(){
        orderService.findOrderByNameSearch(orderAllProducts.getName());
    }
    @Test
    public void testFindOrderByOrderNumber(){
        orderService.findOrderByOrderNumber(orderAllProducts.getOrderNumber());
    }
}
