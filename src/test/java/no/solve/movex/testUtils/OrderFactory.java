package no.solve.movex.testUtils;

import no.solve.movex.domain.Order;
import no.solve.movex.domain.Product;

import java.util.Arrays;
import java.util.List;

/*
Mock order factory to be used in test suites
 */
public abstract class OrderFactory {

    public static Order generateMockOrderWithAllProducts() {
        Order mockOrder = mockDefaultOrderWithoutProduct("Sølve René");
        Product mockProduct = generateMockProduct(true, true, true); //TODO: should put in some descriptive variables
        mockOrder.setProduct(mockProduct);
        return mockOrder;
    }
    public static List<Order> generateListOfHitsByNameLikeSoelve(){
        return mockListOrderByNameLikeSoelveRene();
    }

    private static List<Order> mockListOrderByNameLikeSoelveRene() {
        Order orderSimilarOne = mockDefaultOrderWithoutProduct("Sølve René Stålem");
        Order orderSimilarTwo = mockDefaultOrderWithoutProduct("Sølve René Johnsen");
        Order orderSimilarThree = mockDefaultOrderWithoutProduct("Sølve René Hansen");
        return Arrays.asList(orderSimilarOne, orderSimilarTwo, orderSimilarThree);
    }

    private static Product generateMockProduct(final Boolean isCleaning, final Boolean isMoving, final Boolean isPacking) {
        Product mockProduct =
                Product
                        .builder()
                        .isCleaning(isCleaning)
                        .isMoving(isMoving)
                        .isPacking(isPacking)
                        .build();
        return mockProduct;
    }

    private static Order mockDefaultOrderWithoutProduct(String name) {
        Order mockOrder =
                Order.
                        builder()
                        .movingAddressFrom("Aspervika 81, 4329 Sandnes")
                        .movingAdresssTo("Peter sinnerudsveg 100, 2312 Ottestad")
                        .email("solve.johnsen@gmail.com")
                        .name(name)
                        .phone("45801153")
                        .build();
        return mockOrder;
    }
}
