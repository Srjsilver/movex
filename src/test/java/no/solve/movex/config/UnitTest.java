package no.solve.movex.config;

import no.solve.movex.domain.Order;
import no.solve.movex.testUtils.OrderFactory;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UnitTest {

    protected Order orderAllProducts;

    public UnitTest() {
        orderAllProducts = OrderFactory.generateMockOrderWithAllProducts();
    }
}
