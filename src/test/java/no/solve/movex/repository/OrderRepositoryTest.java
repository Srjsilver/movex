package no.solve.movex.repository;

import no.solve.movex.config.UnitTest;
import no.solve.movex.domain.Order;
import no.solve.movex.testUtils.OrderFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class OrderRepositoryTest extends UnitTest {

    @Mock
    OrderRepository orderRepository;

    @Before
    public void init(){
        when(orderRepository.findOrderByOrderNumber(orderAllProducts.getOrderNumber())).thenReturn(orderAllProducts);
        when(orderRepository.findOrderByNameSearch(orderAllProducts.getName())).thenReturn(OrderFactory.generateListOfHitsByNameLikeSoelve());
        when(orderRepository.placeOrderOrUpdate(orderAllProducts)).thenReturn(123L);
        when(orderRepository.deleteOrder(orderAllProducts.getOrderNumber())).thenReturn(true);
    }

    @Test
    public void placeOrderTest(){
        Long success = orderRepository.placeOrderOrUpdate(orderAllProducts);
        assertTrue(success==123L);

    }
    @Test
    public void deleteOrderTest(){
        Boolean success = orderRepository.deleteOrder(orderAllProducts.getOrderNumber());
        assertTrue(success);

    }
    @Test
    public void findOrderByNameSearchTest(){
        List<Order> orderList = orderRepository.findOrderByNameSearch(orderAllProducts.getName());
        assertNotNull(orderList);
        assertTrue(orderList.size() == 3);
    }
    @Test
    public void findOrderByOrderNumberTest(){
        Order order = orderRepository.findOrderByOrderNumber(orderAllProducts.getOrderNumber());
        assertNotNull(order);
        assertEquals("Sølve René", order.getName());
    }

}
