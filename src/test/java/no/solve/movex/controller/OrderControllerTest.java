package no.solve.movex.controller;

import no.solve.movex.config.UnitTest;
import no.solve.movex.domain.Order;
import no.solve.movex.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrderControllerTest extends UnitTest {

    @InjectMocks
    private OrderController orderController;

    @Mock
    private OrderService orderService;

    @Before
    public void init(){
        when(orderService.findOrderByNameSearch("Sølve")).thenReturn(Arrays.asList(orderAllProducts));
    }

    @Test
    public void placeSimpleOrderTest() {
        orderController.placeOrder(orderAllProducts);
        verify(orderService, times(1)).placeOrder(orderAllProducts);
    }

    @Test
    public void deleteOrderTest() {
        orderController.deleteOrder(orderAllProducts.getOrderNumber());
        verify(orderService, times(1)).deleteOrder(orderAllProducts.getOrderNumber());
    }

    @Test
    public void findOrderBySearchTest() {
        List<Order> listOfOrders = orderController.findOrderByNameSearch("Sølve").getBody();
        verify(orderService, times(1)).findOrderByNameSearch("Sølve");
        assertTrue(listOfOrders.get(0).getName().contains("Sølve"));
    }
}
