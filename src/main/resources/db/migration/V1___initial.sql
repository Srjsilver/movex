CREATE TABLE `t_order` (
  `order_number` bigint(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `moving_address_from` varchar(255) NOT NULL,
  `moving_address_to` varchar(255) NOT NULL,
  `product_is_moving` bit(1) NOT NULL,
  `product_is_packing` bit(1) NOT NULL,
  `product_is_cleaning` bit(1) NOT NULL,
  `comments` mediumtext,
  `carried_out_date` varchar(100) NOT NULL,
  PRIMARY KEY (`order_number`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1