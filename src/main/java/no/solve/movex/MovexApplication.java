package no.solve.movex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovexApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovexApplication.class, args);
    }
}
