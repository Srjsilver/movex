package no.solve.movex.controller;

import no.solve.movex.domain.Order;
import no.solve.movex.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/order")
/**
 * Should configure crossorigin such that it only acceps from specific ports, ip etc..
 */
@CrossOrigin
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/placeOrder")
    public ResponseEntity<Long> placeOrder(@RequestBody Order order) {
        return new ResponseEntity<Long>(orderService.placeOrder(order), HttpStatus.OK);
    }

    @DeleteMapping("/deleteOrder/{orderNumber}")
    public ResponseEntity<Boolean> deleteOrder(@PathVariable Long orderNumber) {
        return new ResponseEntity<Boolean>(orderService.deleteOrder(orderNumber), HttpStatus.OK);
    }


    @GetMapping("/findOrderByNameSearch/{nameLike}")
    public ResponseEntity<List<Order>> findOrderByNameSearch(@PathVariable String nameLike) {
        ResponseEntity<List<Order>> response = new ResponseEntity<List<Order>>(orderService.findOrderByNameSearch(nameLike), HttpStatus.OK);
        return response;
    }

    @GetMapping("/findOrderByNumber/{orderNumber}")
    public ResponseEntity<Order> findOrderByNumber(@PathVariable Long orderNumber) {
        return new ResponseEntity<Order>(orderService.findOrderByOrderNumber(orderNumber), HttpStatus.OK);
    }
}
