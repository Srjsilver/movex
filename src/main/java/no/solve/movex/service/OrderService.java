package no.solve.movex.service;

import lombok.extern.slf4j.Slf4j;
import no.solve.movex.domain.Order;
import no.solve.movex.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public Long placeOrder(Order order) {
        return orderRepository.placeOrderOrUpdate(order);
    }

    public Boolean deleteOrder(Long orderNumber) {
        return orderRepository.deleteOrder(orderNumber);
    }

    public List<Order> findOrderByNameSearch(String nameLike) {
        return orderRepository.findOrderByNameSearch(nameLike);
    }

    public Order findOrderByOrderNumber(Long orderNumber) {
        return orderRepository.findOrderByOrderNumber(orderNumber);
    }
}
