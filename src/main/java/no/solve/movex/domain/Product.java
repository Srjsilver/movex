package no.solve.movex.domain;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@Builder
public class Product {

    private Boolean isMoving;
    private Boolean isPacking;
    private Boolean isCleaning;
}
