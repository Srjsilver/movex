package no.solve.movex.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Order {

    private Long orderNumber;
    private String name;
    private String phone;
    private String email;
    private String movingAddressFrom;
    private String movingAdresssTo;
    private String comments;
    private Date date;
    private String time;
    private Product product;

}
