package no.solve.movex.repository;

import lombok.extern.slf4j.Slf4j;
import no.solve.movex.domain.Order;
import no.solve.movex.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class OrderRepository {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final static String FIND_ORDER_BY_NAME_SEARCH =
            "SELECT * FROM t_order WHERE full_name like :full_name ORDER BY full_name DESC";

    private final static String FIND_ORDER_BY_ORDERNUMBER =
            "SELECT * FROM t_order WHERE order_number = :order_number";

    private final static String FIND_ORDER_WHERE_NAME = "SELECT * FROM t_order WHERE full_name = :name";

    private final static String INSERT_INTO_T_ORDER =
            "INSERT INTO t_order " +
                    "(" +
                    "order_number, full_name, phone_number, email, moving_address_from," +
                    "moving_address_to, product_is_moving, product_is_packing, product_is_cleaning, " +
                    "comments, carried_out_date, carried_out_time)" +
                    "VALUES (null, :name, :phone, :email, :from, :to, :isMoving, :isPacking, :isCleaning, :comments, :cdate, :tTime)";

    private final static String UPDATE_T_ORDER =
            "UPDATE t_order " +
                    "SET full_name = :name, phone_number = :phone, email= :email, moving_address_from = :from, " +
                    "moving_address_to = :to, product_is_moving = :isMoving, product_is_packing = :isPacking, product_is_cleaning = :isCleaning, comments = :comments, carried_out_date = :cdate, carried_out_time = :tTime " +
                    "WHERE order_number = :orderNumber";

    private final static String DELETE_FROM_T_ORDER =
            "DELETE FROM t_order WHERE order_number = ? ";

    @Autowired
    public OrderRepository(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public Long placeOrderOrUpdate(Order orderAllProducts) {
        MapSqlParameterSource namedParams = new MapSqlParameterSource();
        namedParams.addValue("name", orderAllProducts.getName());
        Order order = null;
        KeyHolder key = new GeneratedKeyHolder();
        try {
            order = namedParameterJdbcTemplate.queryForObject(FIND_ORDER_WHERE_NAME, namedParams, getRowMapperForOrder());
        } catch (EmptyResultDataAccessException e) {

        }
        namedParams.addValue("phone", orderAllProducts.getPhone());
        namedParams.addValue("email", orderAllProducts.getEmail());
        namedParams.addValue("from", orderAllProducts.getMovingAddressFrom());
        namedParams.addValue("to", orderAllProducts.getMovingAdresssTo());
        namedParams.addValue("comments", orderAllProducts.getComments());
        namedParams.addValue("cdate", orderAllProducts.getDate());
        namedParams.addValue("tTime", orderAllProducts.getTime());

        Product product = orderAllProducts.getProduct();
        namedParams.addValue("isMoving", product.getIsMoving());
        namedParams.addValue("isPacking", product.getIsPacking());
        namedParams.addValue("isCleaning", product.getIsCleaning());

        if (order != null) {
            namedParams.addValue("orderNumber", order.getOrderNumber());
            namedParameterJdbcTemplate.update(UPDATE_T_ORDER, namedParams, key);
            return order.getOrderNumber();
        } else {
            namedParameterJdbcTemplate.update(INSERT_INTO_T_ORDER, namedParams, key);
            return key.getKey().longValue();
        }


    }

    public Boolean deleteOrder(Long orderNumber) {
        int rowsAfftected = namedParameterJdbcTemplate.getJdbcTemplate().update(DELETE_FROM_T_ORDER, orderNumber);
        return rowsAfftected > 0 ? true : false;
    }

    public List<Order> findOrderByNameSearch(String nameLike) {
        MapSqlParameterSource namedParams = new MapSqlParameterSource("full_name", nameLike + "%");
        return namedParameterJdbcTemplate.query(FIND_ORDER_BY_NAME_SEARCH, namedParams, getRowMapperForOrder());
    }

    public Order findOrderByOrderNumber(Long orderNumber) {
        MapSqlParameterSource namedParams = new MapSqlParameterSource("order_number", orderNumber);
        try {
            Order order = namedParameterJdbcTemplate.queryForObject(FIND_ORDER_BY_ORDERNUMBER, namedParams, getRowMapperForOrder());
            return order;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    private RowMapper<Order> getRowMapperForOrder() {
        return (rs, rowNum) -> {
            Product product = Product.builder()
                    .isMoving(rs.getBoolean("product_is_moving"))
                    .isCleaning(rs.getBoolean("product_is_cleaning"))
                    .isPacking(rs.getBoolean("product_is_packing"))
                    .build();
            Order order = Order.builder()
                    .product(product)
                    .orderNumber(rs.getLong("order_number"))
                    .name(rs.getString("full_name"))
                    .email(rs.getString("email"))
                    .movingAddressFrom(rs.getString("moving_address_from"))
                    .movingAdresssTo(rs.getString("moving_address_to"))
                    .phone(rs.getString("phone_number"))
                    .date(rs.getDate("carried_out_date"))
                    .comments(rs.getString("comments"))
                    .time(rs.getString("carried_out_time"))
                    .build();
            return order;
        };
    }
}
