Author Sølve René Stålem <br>
E-mail: Solve.johnsen@gmail.com


## Movex microservice 
A moving-cleaning-packing service which provides rest endpoints by crud operations.

### To get it starting:

1. Modify your local application-dev<name>.properties and fill in the content of your mariadb connection.
2. Use the database script under resources.db.migration.V1__initial.sql create table script.
3. Modify build.gradle task: bootRun and change spring.profiles to your dev<Name>.
4. In terminal run: gradle bootRun.

#### Rest endpoints according to CRUD

##### /placeOrder handles both create and update by using full name as key

C. http://127.0.0.1:8080/order/placeOrder <br>
R. http://127.0.0.1:8080/order/findOrderByName{nameLike} where nameLike is a string <br>
U. http://127.0.0.1:8080/order/placeOrder <br>
D. http://127.0.0.1:8080/order/deleteOrder/{orderNumber} where orderNumber is the incremental number in your mariaDB table <br>
